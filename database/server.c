#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>


#define PORT 8080

struct allowed{
	char name[10000];
	char password[10000];
};

struct allowed_database{
	char database[10000];
	char name[10000];
};

struct table{
	int jumlahkolom;
	char type[100][10000];
	char data[100][10000];
};

void insertPermission(char *nama, char *database){
	struct allowed_database user;
	strcpy(user.name, nama);
	strcpy(user.database, database);
	printf("%s %s\n", user.name, user.database);
	char fname[]={"databases/permission.dat"};
	FILE *fp;
	fp=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}

int cekAllowedDatabase(char *nama, char *database){
	FILE *fp;
	struct allowed_database user;
	int id,found=0;
	printf("nama = %s  database = %s", nama, database);
	fp=fopen("../database/databases/permission.dat","rb");
	while(1){	
		fread(&user,sizeof(user),1,fp);
		if(strcmp(user.name, nama)==0){
			if(strcmp(user.database, database)==0){
				return 1;
			}
		}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);
	return 0;
}

void createUser(char *nama, char *password){
	struct allowed user;
	strcpy(user.name, nama);
	strcpy(user.password, password);
	printf("%s %s\n", user.name, user.password);
	char fname[]={"databases/user.dat"};
	FILE *fp;
	fp=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}

int cekAllowed(char *username, char *password){
	FILE *fp;
	struct allowed user;
	int id,found=0;
	fp=fopen("../database/databases/user.dat","rb");
	while(1){	
		fread(&user,sizeof(user),1,fp);
		if(strcmp(user.name, username)==0){
			if(strcmp(user.password, password)==0){
				found=1;
			}
		}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);
	if(found==0){
		printf("You're Not Allowed\n");
		return 0;
	}else{
		return 1;
	}
}

int main(){

	int sockfd, ret;
	 struct sockaddr_in serverAddr;

	int newSocket;
	struct sockaddr_in newAddr;

	socklen_t addr_size;

	char buffer[1024];
	pid_t childpid;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0){
		printf("[-]Error in connection.\n");
		exit(1);
	}
	printf("[+]Server Socket is created.\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = bind(sockfd, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
	if(ret < 0){
		printf("[-]Error in binding.\n");
		exit(1);
	}
	printf("[+]Bind to port %d\n", 4444);

	if(listen(sockfd, 10) == 0){
		printf("[+]Listening....\n");
	}else{
		printf("[-]Error in binding.\n");
	}


	while(1){
		newSocket = accept(sockfd, (struct sockaddr*)&newAddr, &addr_size);
		if(newSocket < 0){
			exit(1);
		}
		printf("Connection accepted from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));

		if((childpid = fork()) == 0){
			close(sockfd);

			while(1){
				recv(newSocket, buffer, 1024, 0);
				printf("buffer %s\n", buffer);
                char *token;
				char buffercopy[32000];
				strcpy(buffercopy, buffer);
				char perintah[100][10000];
                token = strtok(buffercopy, ":");
                int i=0;
                char database_used[1000];
                while( token != NULL ) {
                    strcpy(perintah[i], token);
                    i++;
                    token = strtok(NULL, ":");
                }
                if(strcmp(perintah[0], "cUser")==0){
                    if(strcmp(perintah[3], "0")==0){
                        createUser(perintah[1], perintah[2]);
                    }else{
                        char peringatan[] = "You're Not Allowed";
                        send(newSocket, peringatan, strlen(peringatan), 0);
                        bzero(buffer, sizeof(buffer));
                    }
                }
                else if(strcmp(perintah[0], "uDatabase") == 0){
                    if(strcmp(perintah[3], "0") != 0){
                        int allowed = cekAllowedDatabase(perintah[2], perintah[1]);
                        if(allowed != 1){
                            char peringatan[] = "Access_database : You're Not Allowed";
                            send(newSocket, peringatan, strlen(peringatan), 0);
                            bzero(buffer, sizeof(buffer));
                        }else{
                            strncpy(database_used, perintah[1], sizeof(perintah[1]));
                            char peringatan[] = "Access_database : Allowed";
                            printf("database_used = %s\n", database_used);
                            send(newSocket, peringatan, strlen(peringatan), 0);
                            bzero(buffer, sizeof(buffer));
                        }
                    }
                }
                else if(strcmp(perintah[0], "cDatabase")==0){
                    char lokasi[20000];
                    snprintf(lokasi, sizeof lokasi, "databases/%s", perintah[1]);
                    printf("lokasi = %s, nama = %s , database = %s\n", lokasi, perintah[2], perintah[1]);
                    mkdir(lokasi,0777);
                    insertPermission(perintah[2], perintah[1]);
                }
                else if(strcmp(perintah[0], "cTable")==0){
					printf("%s\n", perintah[1]);
					char *tokens;
					if(database_used[0] == '\0'){
						strcpy(database_used, "You're not selecting database yet");
						send(newSocket, database_used, strlen(database_used), 0);
						bzero(buffer, sizeof(buffer));
                    }else{
                        char daftarQuery[100][10000];
                        char copyPerintah[20000];
                        snprintf(copyPerintah, sizeof copyPerintah, "%s", perintah[1]);
                        tokens = strtok(copyPerintah, "(), ");
                        int jumlah=0;
                        while( tokens != NULL ) {
                            strcpy(daftarQuery[jumlah], tokens);
                            printf("%s\n", daftarQuery[jumlah]);
                            jumlah++;
                            tokens = strtok(NULL, "(), ");
                        }
                        char buatTable[20000];
                        snprintf(buatTable, sizeof buatTable, "../database/databases/%s/%s", database_used, daftarQuery[2]);
                        int iterasi = 0;
                        int iterasiData = 3;
                        struct table kolom;
                        while(jumlah > 3){
                            strcpy(kolom.data[iterasi], daftarQuery[iterasiData]);
                            printf("%s\n", kolom.data[iterasi]);
                            strcpy(kolom.type[iterasi], daftarQuery[iterasiData+1]);
                            iterasiData = iterasiData+2;
                            jumlah=jumlah-2;
                            iterasi++;
                        }
                        kolom.jumlahkolom = iterasi;
                        printf("iterasi = %d\n", iterasi);
                        FILE *fp;
                        printf("%s\n", buatTable);
                        fp=fopen(buatTable,"ab");
                        fwrite(&kolom,sizeof(kolom),1,fp);
                        fclose(fp);
                    }
                                }else if(strcmp(perintah[0], "dDatabase")==0){
                            int allowed = cekAllowedDatabase(perintah[2], perintah[1]);
                            if(allowed != 1){
                                char peringatan[] = "Access_database : You're Not Allowed";
                                send(newSocket, peringatan, strlen(peringatan), 0);
                                bzero(buffer, sizeof(buffer));
                                continue;
                            }else{
                                char hapus[20000];
                                snprintf(hapus, sizeof hapus, "rm -r databases/%s", perintah[1]);
                                system(hapus);
                                char peringatan[] = "Database Has Been Removed";
                                send(newSocket, peringatan, strlen(peringatan), 0);
                                bzero(buffer, sizeof(buffer));
                            }				
                                }else if(strcmp(perintah[0], "dDatabase")==0){
                            int allowed = cekAllowedDatabase(perintah[2], perintah[1]);
                            if(allowed != 1){
                                char peringatan[] = "Access_database : You're Not Allowed";
                                send(newSocket, peringatan, strlen(peringatan), 0);
                                bzero(buffer, sizeof(buffer));
                                continue;
                            }else{
                                char hapus[20000];
                                snprintf(hapus, sizeof hapus, "rm -r databases/%s", perintah[1]);
                                system(hapus);
                                char peringatan[] = "Database Has Been Removed";
                                send(newSocket, peringatan, strlen(peringatan), 0);
                                bzero(buffer, sizeof(buffer));
                            }
                                }else if(strcmp(perintah[0], "dColumn")==0){
                            if(database_used[0] == '\0'){
                                strcpy(database_used, "You're not selecting database yet");
                                send(newSocket, database_used, strlen(database_used), 0);
                                bzero(buffer, sizeof(buffer));
                                continue;
                            }
                            char buatTable[20000];
                            snprintf(buatTable, sizeof buatTable, "databases/%s/%s", database_used, perintah[2]);
                            int index = findColumn(buatTable, perintah[1]);
                            if(index == -1){
                                char peringatan[] = "Column Not Found";
                                send(newSocket, peringatan, strlen(peringatan), 0);
                                bzero(buffer, sizeof(buffer));
                                continue;
                            }
					deleteColumn(buatTable, index);
					char peringatan[] = "Column Has Been Removed";
					send(newSocket, peringatan, strlen(peringatan), 0);
					bzero(buffer, sizeof(buffer));
				if(strcmp(buffer, ":exit") == 0){
					printf("Disconnected from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
					break;
				}else{
					printf("Client: %s\n", buffer);
					send(newSocket, buffer, strlen(buffer), 0);
					bzero(buffer, sizeof(buffer));
				}
			}
		}

	}
	close(newSocket);
	return 0;
}
