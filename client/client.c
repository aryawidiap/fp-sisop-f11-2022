#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>

#define PORT 8080

int main(int argc, char *argv[]){
    int allowed=0;
	int user_id = geteuid();
	char database_used[1000];
	if(user_id == 0){
		allowed=1;
	}else{
		allowed = cekAllowed(argv[2],argv[4]);
	}
	if(allowed==0){
		return 0;
	}
	int clientSocket, ret;
	struct sockaddr_in serverAddr;
	char buffer[32000];

	clientSocket = socket(AF_INET, SOCK_STREAM, 0);
	if(clientSocket < 0){
		printf("[-]Error in connection.\n");
		exit(1);
	}
	printf("[+]Client Socket is created.\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = connect(clientSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
	if(ret < 0){
		printf("[-]Error in connection.\n");
		exit(1);
	}
	printf("[+]Connected to Server.\n");
	while(1){
		printf("Client: \t");
		char input[10000];
        char copyinput[10000];
		char perintah[100][10000];
		char *token;
		int i=0;
		scanf(" %[^\n]s", input);
		printf("ini input %s\n", input);
		token = strtok(input, " ");
		while( token != NULL ) {
			strcpy(perintah[i], token);
			i++;
			token = strtok(NULL, " ");
		}
        int wrongCommand = 0;
		if(strcmp(perintah[0], "CREATE")==0){
			if(strcmp(perintah[1], "USER")==0 && strcmp(perintah[3], "IDENTIFIED")==0 && strcmp(perintah[4], "BY")==0){
				snprintf(buffer, sizeof buffer, "cUser:%s:%s:%d", perintah[2], perintah[5], user_id);
				send(clientSocket, buffer, strlen(buffer), 0);
			}else if(strcmp(perintah[1], "DATABASE")==0){
				snprintf(buffer, sizeof buffer, "cDatabase:%s:%s:%d", perintah[2], argv[2], user_id);
				send(clientSocket, buffer, strlen(buffer), 0);
			}else if(strcmp(perintah[1], "TABLE")==0){
				snprintf(buffer, sizeof buffer, "cTable:%s", copyinput);
				send(clientSocket, buffer, strlen(buffer), 0);
			}
		}else if(strcmp(perintah[0], "GRANT")==0 && strcmp(perintah[1], "PERMISSION")==0 && strcmp(perintah[3], "INTO")==0){
			snprintf(buffer, sizeof buffer, "gPermission:%s:%s:%d", perintah[2],perintah[4], user_id);
			send(clientSocket, buffer, strlen(buffer), 0);
		}else if(strcmp(perintah[0], "USE")==0){
			snprintf(buffer, sizeof buffer, "uDatabase:%s:%s:%d", perintah[1], argv[2], user_id);
			send(clientSocket, buffer, strlen(buffer), 0);
		}else if(strcmp(perintah[0], "cekCurrentDatabase")==0){
			snprintf(buffer, sizeof buffer, "%s", perintah[0]);
			send(clientSocket, buffer, strlen(buffer), 0);
		}else if(strcmp(perintah[0], "DROP")==0){
			if(strcmp(perintah[1], "DATABASE")==0){
				snprintf(buffer, sizeof buffer, "dDatabase:%s:%s", perintah[2], argv[2]);
				send(clientSocket, buffer, strlen(buffer), 0);
			}else if(strcmp(perintah[1], "TABLE")==0){
				snprintf(buffer, sizeof buffer, "dTable:%s:%s", perintah[2], argv[2]);
				send(clientSocket, buffer, strlen(buffer), 0);
			}else if(strcmp(perintah[1], "COLUMN")==0){
				snprintf(buffer, sizeof buffer, "dColumn:%s:%s:%s", perintah[2], perintah[4] ,argv[2]);
				send(clientSocket, buffer, strlen(buffer), 0);
			}
		}else if(strcmp(perintah[0], "INSERT")==0 && strcmp(perintah[1], "INTO")==0){
            snprintf(buffer, sizeof buffer, "insert:%s", copyinput);
			send(clientSocket, buffer, strlen(buffer), 0);
        }else if(strcmp(perintah[0], "UPDATE")==0){
            snprintf(buffer, sizeof buffer, "update:%s", copyinput);
			send(clientSocket, buffer, strlen(buffer), 0);
        }else if(strcmp(perintah[0], "DELETE")==0){
            snprintf(buffer, sizeof buffer, "delete:%s", copyinput);
            send(clientSocket, buffer, strlen(buffer), 0);
        }else if(strcmp(perintah[0], "SELECT")==0){
            snprintf(buffer, sizeof buffer, "select:%s", copyinput);
            send(clientSocket, buffer, strlen(buffer), 0);
        }else if(strcmp(perintah[0], ":exit")!=0){
			wrongCommand = 1;
			char peringatan[] = "Wrong Command";
			send(clientSocket, peringatan, strlen(peringatan), 0);
		}

		if(wrongCommand != 1){
			char namaSender[10000];
			if(user_id == 0){
				strcpy(namaSender, "root");
			}else{
				strcpy(namaSender, argv[2]);
			}
			writelog(copyinput, namaSender);
		}
		if(strcmp(perintah[0], ":exit") == 0){
			close(clientSocket);
			printf("[-]Disconnected from server.\n");
			exit(1);
		}

		if(recv(clientSocket, buffer, 1024, 0) < 0){
			printf("[-]Error in receiving data.\n");
		}else{
			printf("Server: \t%s\n", buffer);
		}
	}

	return 0;
}
